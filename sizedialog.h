#ifndef GRIDSIZEDIALOG_H
#define GRIDSIZEDIALOG_H

#include <QDialog>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>

class SizeDialog : public QDialog // rács méretét beállító dialógus
{
  Q_OBJECT
public:
  explicit SizeDialog(QWidget *parent = 0);

  int getSize() const;

private slots:
  void setter(int n);

private:
  int size;
  QLabel *text;
  QVBoxLayout *mainLayout;
  QPushButton *sizeS;
  QPushButton *sizeM;
  QPushButton *sizeL;
  QPushButton *sizeRandom;
  QPushButton *cancel;
  QGridLayout *gridLayout;
};

#endif // GRIDSIZEDIALOG_H
