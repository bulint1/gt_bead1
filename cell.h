#ifndef CELL_H
#define CELL_H
#include "player.h"
#include <QColor>
class Cell
{
    int cellScore;
    int owner;
    QColor color;
public:
    Cell();
    void reset();
    void increase(Player* inc);
    int getCellScore() const;
    void setCellScore(int value);
    int getOwner() const;
    void setOwner(int value);
    QColor getColor() const;
    void setColor(const QColor &value);
};

#endif // CELL_H
