#include "cell.h"
Cell::Cell() {
  cellScore = 0;
  color = "#FFFFFF";
}

void Cell::reset() {
  cellScore = 0;
  owner = 0;
  color = "#FFFFFF";
}

void Cell::increase(Player *inc) {
  if (cellScore < 4) {
    ++cellScore;
    if (cellScore == 4) {
      inc->score++;
      color = inc->color;
    }
  }
}

int Cell::getCellScore() const { return cellScore; }

void Cell::setCellScore(int value) { cellScore = value; }

QColor Cell::getColor() const { return color; }

void Cell::setColor(const QColor &value) { color = value; }
