#ifndef GAMEWIDGET_H
#define GAMEWIDGET_H

#include "cell.h"
#include "player.h"
//#include "sizedialog.h"
#include "sizedialog.h"
#include <QGridLayout>
#include <QGroupBox>
#include <QLCDNumber>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>
#include <QVBoxLayout>
#include <QWidget>

class GameWidget : public QWidget {
  Q_OBJECT

public:
  GameWidget(QWidget *parent = nullptr);

private slots:
  void buttonClicked();        // táblagombra kattintás esménykezelője
  void newGameButtonClicked(); // új játék gombra kattintás eseménykezelője

private:
  int boardSize;
  Player *currentPlayer;
  Player *p1;
  Player *p2;
  QLabel *p1Label;
  QLabel *p2Label;
  QLCDNumber *p1Score;
  QLCDNumber *p2Score;
  // sizeDialog *dialog;
  SizeDialog *dialog;
  QGridLayout *tableLayout;
  QHBoxLayout *topLayout;
  QGroupBox *horizontalGroupBox;
  QVBoxLayout *mainLayout;
  QPushButton *newGameButton;                  // új játék gombja
  QVector<QVector<QPushButton *>> buttonTable; // gombtábla
  Cell **gameTable;                            // játéktábla
  void changePlayerTurn();
  void updateButtons();
  void newGame();              // új játék kezdése
  void increase(int x, int y); // játék léptetése
  void generateTable(int n);   // tábla létrehozása
  void checkGame();            // vége van-e a játéknak
};

#endif // GAMEWIDGET_H
