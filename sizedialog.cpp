#include "sizedialog.h"

SizeDialog::SizeDialog(QWidget *parent) : QDialog(parent) {
  setFixedSize(200, 100);
  setWindowTitle("Options");
  setModal(true);
  size = 5;
  srand(time(nullptr));

  text = new QLabel("Select the size of the Game!");
  sizeS = new QPushButton("3x3");
  sizeM = new QPushButton("5x5");
  sizeL = new QPushButton("7x7");
  sizeRandom = new QPushButton("Random");
  cancel = new QPushButton("Cancel");
  connect(sizeS, &QPushButton::clicked, this, [this] { setter(3); });
  connect(sizeS, SIGNAL(clicked()), this, SLOT(accept()));
  connect(sizeM, &QPushButton::clicked, this, [this] { setter(5); });
  connect(sizeM, SIGNAL(clicked()), this, SLOT(accept()));
  connect(sizeL, &QPushButton::clicked, this, [this] { setter(7); });
  connect(sizeL, SIGNAL(clicked()), this, SLOT(accept()));
  connect(sizeRandom, &QPushButton::clicked, this, [this] { setter(42); });
  connect(sizeRandom, SIGNAL(clicked()), this, SLOT(accept()));

  gridLayout = new QGridLayout;
  mainLayout = new QVBoxLayout;
  gridLayout->addWidget(sizeS, 0, 0);
  gridLayout->addWidget(sizeM, 0, 1);
  gridLayout->addWidget(sizeL, 1, 0);
  gridLayout->addWidget(sizeRandom, 1, 1);
  mainLayout->addWidget(text);
  mainLayout->addLayout(gridLayout);

  setLayout(mainLayout);
}
void SizeDialog::setter(int n) {
  if (n == 42) {
    // static const int options[] = {3, 5, 7};
    int random = rand() % 3;
    size = 3 + 2 * random;
  } else
    size = n;
  this->close();
}

int SizeDialog::getSize() const { return size; }
