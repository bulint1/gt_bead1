#include "gamewidget.h"
#include <iostream>
//#include <QFontDatabase>

GameWidget::GameWidget(QWidget *parent) {
  setMinimumSize(500, 500);
  setBaseSize(500, 500);
  setMaximumSize(600, 600);
  setWindowTitle("Game of Fours");
  // QFontDatabase::addApplicationFont(":/fonts/TallyMark.ttf");

  p1Label = new QLabel("Red Player");
  p1Score = new QLCDNumber;
  p1Label->setAlignment(Qt::AlignCenter);
  p2Label = new QLabel("Blue Player");
  p2Score = new QLCDNumber;
  p2Label->setAlignment(Qt::AlignCenter);
  p1 = new Player("#D32F2F");
  p2 = new Player("#448AFF");
  currentPlayer = p1;

  // dialog = new sizeDialog();
  dialog = new SizeDialog(this);

  newGameButton = new QPushButton("New Game");
  // connect(newGameButton, &QPushButton::clicked, this, [this] {
  // newGameButtonClicked(); });
  connect(newGameButton, SIGNAL(clicked()), this, SLOT(newGameButtonClicked()));
  topLayout = new QHBoxLayout();
  topLayout->addWidget(p1Label);
  topLayout->addWidget(p1Score);
  topLayout->addWidget(newGameButton);
  topLayout->addWidget(p2Score);
  topLayout->addWidget(p2Label);

  horizontalGroupBox = new QGroupBox();
  horizontalGroupBox->setLayout(topLayout);
  horizontalGroupBox->setMaximumHeight(50);

  tableLayout = new QGridLayout();

  generateTable(5);
  boardSize = buttonTable.size();

  mainLayout = new QVBoxLayout();
  mainLayout->addWidget(horizontalGroupBox);
  mainLayout->addLayout(tableLayout);
  setLayout(mainLayout);
  // resize(0, 0);

  // newGame();
}
void GameWidget::buttonClicked() {
  QPushButton *senderButton = dynamic_cast<QPushButton *>(QObject::sender());
  int location = tableLayout->indexOf(senderButton);

  increase(location / boardSize, location % boardSize);
}

void GameWidget::newGameButtonClicked() // Dialog to select size with four
                                        // buttuns(3,5,7,cancel)
{
  if (dialog->exec() == QDialog::Accepted) {
    newGame();
  }
}

void GameWidget::newGame() {
  int temp = boardSize;
  boardSize = dialog->getSize();
  for (int i = 0; i < temp; ++i) {
    for (int j = 0; j < temp; ++j) {
      tableLayout->removeWidget(buttonTable[i][j]);
      delete buttonTable[i][j];
      // mainLayout->SetMinimumSize();

      // gameTable[i][j].reset();
      // buttonTable[i][j]->setEnabled(true);
    }
  }
  // dialog->exec();
  // mainLayout->setSizeConstraint(mainLayout->SetMinimumSize);
  p1->score = 0;
  p2->score = 0;
  p1Score->display(p1->score);
  p2Score->display(p2->score);
  generateTable(boardSize); // variable to change size
  setMinimumSize(boardSize * 100, boardSize * 100);
  setMaximumSize((boardSize + 1) * 100, (boardSize + 1) * 100);
  // resize(0, 0);
}

void GameWidget::changePlayerTurn() {
  if (currentPlayer == p1)
    currentPlayer = p2;
  else
    currentPlayer = p1;
}

void GameWidget::updateButtons() {
  for (int i = 0; i < boardSize; ++i) {
    for (int j = 0; j < boardSize; ++j) {
      if (gameTable[i][j].getColor() != "#FFFFFF") {
        QPalette pal = buttonTable[i][j]->palette();
        pal.setColor(QPalette::Button, gameTable[i][j].getColor());
        buttonTable[i][j]->setPalette(pal);
      }
      if (gameTable[i][j].getCellScore() == 4) {
        buttonTable[i][j]->setDisabled(true);
      }
    }
  }
}

void GameWidget::increase(int x, int y) {
  if (x < boardSize - 1) {
    gameTable[x + 1][y].increase(currentPlayer);
    buttonTable[x + 1][y]->setText(
        QString::number(gameTable[x + 1][y].getCellScore()));
  }
  if (y < boardSize - 1) {
    gameTable[x][y + 1].increase(currentPlayer);
    buttonTable[x][y + 1]->setText(
        QString::number(gameTable[x][y + 1].getCellScore()));
  }
  if (x > 0) {
    gameTable[x - 1][y].increase(currentPlayer);
    buttonTable[x - 1][y]->setText(
        QString::number(gameTable[x - 1][y].getCellScore()));
  }
  if (y > 0) {
    gameTable[x][y - 1].increase(currentPlayer);
    buttonTable[x][y - 1]->setText(
        QString::number(gameTable[x][y - 1].getCellScore()));
  }
  gameTable[x][y].increase(currentPlayer);
  buttonTable[x][y]->setText(QString::number(gameTable[x][y].getCellScore()));

  p1Score->display(p1->score);
  p2Score->display(p2->score);
  updateButtons();
  changePlayerTurn();
  checkGame();
}

void GameWidget::generateTable(int n) {
  gameTable = new Cell *[n];
  buttonTable.resize(n);

  for (int i = 0; i < n; ++i) {
    gameTable[i] = new Cell[n];
    buttonTable[i].resize(n);
    for (int j = 0; j < n; ++j) {
      buttonTable[i][j] = new QPushButton(this);
      buttonTable[i][j]->setFont(QFont("Times New Roman", 60, QFont::Normal));
      // buttonTable[i][j]->setSizePolicy(QSizePolicy::Ignored,
      // QSizePolicy::Ignored);
      buttonTable[i][j]->setText("0");
      tableLayout->addWidget(buttonTable[i][j], i,
                             j); // gombok felvétele az elhelyezésbe

      connect(buttonTable[i][j], SIGNAL(clicked()), this,
              SLOT(buttonClicked()));
    }
  }
}

void GameWidget::checkGame() {
  if (((p1->score) + (p2->score)) == (boardSize * boardSize)) {
    if (p1->score > p2->score)
      QMessageBox::information(this, "Game Over!", "The Red Player won!");
    else
      QMessageBox::information(this, "Game Over!", "The Blue Player won!");
    dialog->exec();
    newGame();
  }
}
