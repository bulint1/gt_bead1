#ifndef STRUCTS_H
#define STRUCTS_H
#include <QColor>
struct Player {
  QColor color;
  int score;
  Player(QColor col) {
    score = 0;
    color = col;
  }
};
#endif // STRUCTS_H
